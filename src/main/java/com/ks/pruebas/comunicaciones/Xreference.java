package com.ks.pruebas.comunicaciones;

/**
 * Created by Miguel on 14/07/2017.
 */
import com.ks.lib.tcp.Cliente;
import com.ks.lib.tcp.EventosTCP;
import com.ks.lib.tcp.Tcp;

public class Xreference extends Cliente implements EventosTCP
{
    private static Xreference instancia;

    private static int numeroClase = 0;

    private int miNumero;



    private Xreference()
    {
        this.setEventos(this);
        numeroClase += 1;
        miNumero = numeroClase;
    }

    public void conexionEstablecida(Cliente cliente)
    {
        System.out.println(miNumero + " Se conecto el cliente: " + cliente.toString());
    }

    public void errorConexion(String s)
    {
        System.out.println(miNumero + " Error de conexion: " + s);
    }

    public void datosRecibidos(String s, byte[] bytes, Tcp tcp)
    {
        System.out.println(miNumero + " Este es el mensaje que llego: " + s);
    }

    public void cerrarConexion(Cliente cliente)
    {
        System.out.println(miNumero + " Se cerro la conexion con el cliente " + cliente.toString());
    }

    public static Xreference getInstancia()
    {
        if (instancia == null)
        {
            instancia = new Xreference();
        }
        return instancia;
    }

    public void solicitarReferencia(String tarjeta)
    {
        if (tarjeta.length() > 15)
        {
            String VLstrMensaje = "SOINTER    0020 1 " + tarjeta;
            this.enviar(VLstrMensaje);
        }
    }
}
